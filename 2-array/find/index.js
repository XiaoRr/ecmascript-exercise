export default function find00OldPerson(collection) {
  // TODO 4: 在这里写实现代码
  return collection
    .filter(x => {
      return x.age <= 19 && x.age > 9;
    })
    .map(x => x.name)[0];
}
